#ifndef __BME280_VENDOR__
#define __BME280_VENDOR__

#include <stdint.h>

typedef int32_t BME280_S32_t;

// Returns temperature in DegC, double precision. Output value of "51.23" equals 51.23 DegC.
// t_fine carries fine temperature
double BME280_compensate_T_double(
	const BME280_S32_t adc_T,
	const uint16_t dig_T1,
	const int16_t dig_T2,
	const int16_t dig_T3,
	BME280_S32_t *t_fine
);

// Returns pressure in Pa as double. Output value of "96386.2" equals 96386.2 Pa = 963.862 hPa
double BME280_compensate_P_double(
	const BME280_S32_t adc_P,
	const uint16_t dig_P1,
	const int16_t dig_P2,
	const int16_t dig_P3,
	const int16_t dig_P4,
	const int16_t dig_P5,
	const int16_t dig_P6,
	const int16_t dig_P7,
	const int16_t dig_P8,
	const int16_t dig_P9,
	const BME280_S32_t t_fine
);

// Returns humidity in %rH as as double. Output value of "46.332" represents 46.332 %rH
double BME280_compensate_H_double(
	const BME280_S32_t adc_H,
	const uint8_t dig_H1,
	const int16_t dig_H2,
	const uint8_t dig_H3,
	const int16_t dig_H4,
	const int16_t dig_H5,
	const int8_t dig_H6,
	const BME280_S32_t t_fine
);

#endif