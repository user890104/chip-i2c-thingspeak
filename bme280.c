#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>

#include "i2c.h"
#include "bme280-vendor.h"

enum bme280_reg_addrs {
	BME280_CALIB00 = 0x88,
	/*
	...
	*/
	BME280_CALIB25 = 0xA1,
	BME280_CHIP_ID = 0xD0,
	BME280_RESET = 0xE0,
	BME280_CALIB26,
	/*
	...
	*/
	BME280_CALIB41 = 0xF0,
	BME280_CTRL_HUM = 0xF2,
	BME280_STATUS,
	BME280_CTRL_MEAS,
	BME280_CONFIG,
	BME280_PRESS_MSB = 0xF7,
	BME280_PRESS_LSB,
	BME280_PRESS_XLSB,
	BME280_TEMP_MSB,
	BME280_TEMP_LSB,
	BME280_TEMP_XLSB,
	BME280_HUM_MSB,
	BME280_HUM_LSB,
};

#define BME280_MODE_SLEEP 0x00
#define BME280_MODE_FORCED 0x01
#define BME280_MODE_NORMAL 0x03

#define BME280_CMD_RESET 0xB6

#define BME280_CALIB_TO_16BIT(ptr, idx) ((ptr[idx + 1] << 8) | ptr[idx])

#define BME280_MEAS_TO_16BIT(ptr, idx) ((ptr[idx] << 8) | ptr[idx + 1])
#define BME280_MEAS_TO_24BIT(ptr, idx) ((int32_t)(((ptr[idx] << 16) | (ptr[idx + 1] << 8) | ptr[idx + 2]) >> 4))

#ifdef DEBUG2
static void dump_hex(const char *data, int len) {
	int i;

	for (i = 0; i < len; ++i) {
		printf("%02x ", data[i]);
	}

	puts("");
}
#endif

static unsigned int bme280_read(int file, enum bme280_reg_addrs reg_addr, char *rdata, size_t rlen) {
	const char wdata[1] = { reg_addr };
	int result = i2c_rw(file, wdata, 1, rdata, rlen);
	
	#ifdef DEBUG2
	fprintf(stderr, "READ 0x%02x\n", reg_addr);
	dump_hex(rdata, rlen);
	#endif
	
	if (result != 0) {
		fprintf(stderr, "ERROR READING!\n");
	}
	
	return result;
}

static unsigned int bme280_write(int file, enum bme280_reg_addrs reg_addr, const char *wdata, size_t wlen) {
	#ifdef DEBUG2
	fprintf(stderr, "WRITE 0x%02x\n", reg_addr);
	dump_hex(wdata, wlen);
	#endif
	
	char *packet = calloc(wlen + 1, sizeof(char));
	packet[0] = reg_addr;
	memcpy(packet + 1, wdata, wlen);
	unsigned int result = i2c_rw(file, packet, wlen + 1, NULL, 0);
	free(packet);
	
	if (result != 0) {
		fprintf(stderr, "ERROR WRITING!\n");
	}
	
	return result;
}

static int bme280_get_callib(int file, char *dig, unsigned char first, unsigned char last) {
	if (bme280_read(file, first, dig, last + 1 - first)) {
		return 1;
	}

	return 0;
}

static int bme280_print_part(int file) {
	char rdata[1];

	if (bme280_read(file, BME280_CHIP_ID, rdata, 1)) {
		return 1;
	}

	fputs("PART: ", stderr);

	switch (rdata[0]) {
		case 0x56:
		case 0x57:
			fputs("BMP280 (sample)", stderr);
			break;
		case 0x58:
			fputs("BMP280", stderr);
			break;
		case 0x60:
			fputs("BME280", stderr);
			break;
		default:
			fputs("UNKNOWN", stderr);
			break;
	}

	fputs("\n", stderr);

	return 0;
}

static int bme280_reset(int file) {
	const char wdata[1] = { BME280_CMD_RESET };

	if (bme280_write(file, BME280_RESET, wdata, 1)) {
		return 1;
	}

	return 0;
}

#ifdef DEBUG
static int bme280_get_control_hum(int file, char *osrs_h) {
	char rdata;

	if (bme280_read(file, BME280_CTRL_HUM, &rdata, 1)) {
		return 1;
	}

	*osrs_h = rdata & 0x07;

	return 0;
}
#endif

static int bme280_set_control_hum(int file, const char osrs_h) {
	char wdata = osrs_h & 0x07;

	if (bme280_write(file, BME280_CTRL_HUM, &wdata, 1)) {
		return 1;
	}

	return 0;
}

#ifdef DEBUG
static int bme280_get_status(int file, char *measuring, char *im_update) {
	char rdata;

	if (bme280_read(file, BME280_STATUS, &rdata, 1)) {
		return 1;
	}

	*measuring = (rdata & 0x08) >> 3;
	*im_update = rdata & 0x01;

	return 0;
}

static int bme280_get_control_meas(int file, char *mode, char *osrs_p, char *osrs_t) {
	char rdata;

	if (bme280_read(file, BME280_CTRL_MEAS, &rdata, 1)) {
		return 1;
	}

	*mode = rdata & 0x03;
	*osrs_p = (rdata >> 2) & 0x07;
	*osrs_t = (rdata >> 5) & 0x07;

	return 0;
}
#endif

static int bme280_set_control_meas(int file, const char mode, const char osrs_p, const char osrs_t) {
	char wdata = ((osrs_t & 0x07) << 5) | ((osrs_p & 0x07) << 2) | (mode & 0x03);

	if (bme280_write(file, BME280_CTRL_MEAS, &wdata, 1)) {
		return 1;
	}

	return 0;
}

#ifdef DEBUG
static int bme280_get_config(int file, char *spi3w_en, char *filter, char *t_sb) {
	char rdata;

	if (bme280_read(file, BME280_CONFIG, &rdata, 1)) {
		return 1;
	}

	*spi3w_en = rdata & 0x01;
	*filter = (rdata >> 2) & 0x07;
	*t_sb = (rdata >> 5) & 0x07;

	return 0;
}
#endif

static int bme280_set_config(int file, const char spi3w_en, const char filter, const char t_sb) {
	char wdata = ((t_sb & 0x07) << 5) | ((filter & 0x07) << 2) | (spi3w_en & 0x01);

	if (bme280_write(file, BME280_CONFIG, &wdata, 1)) {
		return 1;
	}

	return 0;
}

#ifdef DEBUG
static int bme280_print_state(int file) {
	// CONTROL_HUM
	char osrs_h;

	if (bme280_get_control_hum(file, &osrs_h)) {
		return 1;
	}

	fprintf(stderr, "osrs_h = %u\n", osrs_h);

	// STATUS
	char measuring, im_update;

	if (bme280_get_status(file, &measuring, &im_update)) {
		return 1;
	}

	fprintf(stderr, "measuring = %u, im_update = %u\n", measuring, im_update);

	// CONTROL_MEAS
	char mode, osrs_p, osrs_t;

	if (bme280_get_control_meas(file, &mode, &osrs_p, &osrs_t)) {
		return 1;
	}

	fprintf(stderr, "mode = %u, osrs_p = %u, osrs_t = %u\n", mode, osrs_p, osrs_t);

	// CONFIG
	char spi3w_en, filter, t_sb;

	if (bme280_get_config(file, &spi3w_en, &filter, &t_sb)) {
		return 1;
	}

	fprintf(stderr, "spi3w_en = %u, filter = %u, t_sb = %u\n", spi3w_en, filter, t_sb);

	return 0;
}
#endif

static int bme280_get_readings(int file, int32_t *press, int32_t *temp, int32_t *hum) {
	char rdata[8];

	if (bme280_read(file, BME280_PRESS_MSB, rdata, 8)) {
		return 1;
	}

	*press = BME280_MEAS_TO_24BIT(rdata, 0);
	*temp = BME280_MEAS_TO_24BIT(rdata, 3);
	*hum = BME280_MEAS_TO_16BIT(rdata, 6);

	return 0;
}

static int bme280_get_env(int file, char *buf) {
	if (bme280_reset(file)) {
		return 1;
	}

	usleep(2 * 1000); // 2 ms

	char digTP[26];
	char digH[16];

	if (bme280_get_callib(file, digTP, BME280_CALIB00, BME280_CALIB25)) {
		return 1;
	}

	if (bme280_get_callib(file, digH, BME280_CALIB26, BME280_CALIB41)) {
		return 1;
	}

	uint16_t dig_T1 = BME280_CALIB_TO_16BIT(digTP, 0);
	int16_t dig_T2 = BME280_CALIB_TO_16BIT(digTP, 2);
	int16_t dig_T3 = BME280_CALIB_TO_16BIT(digTP, 4);

	uint16_t dig_P1 = BME280_CALIB_TO_16BIT(digTP, 6);
	int16_t dig_P2 = BME280_CALIB_TO_16BIT(digTP, 8);
	int16_t dig_P3 = BME280_CALIB_TO_16BIT(digTP, 10);
	int16_t dig_P4 = BME280_CALIB_TO_16BIT(digTP, 12);
	int16_t dig_P5 = BME280_CALIB_TO_16BIT(digTP, 14);
	int16_t dig_P6 = BME280_CALIB_TO_16BIT(digTP, 16);
	int16_t dig_P7 = BME280_CALIB_TO_16BIT(digTP, 18);
	int16_t dig_P8 = BME280_CALIB_TO_16BIT(digTP, 20);
	int16_t dig_P9 = BME280_CALIB_TO_16BIT(digTP, 22);

	uint8_t dig_H1 = digTP[25];
	int16_t dig_H2 = BME280_CALIB_TO_16BIT(digH, 0);
	uint8_t dig_H3 = digH[2];
	int16_t dig_H4 = (digH[3] << 4) | (digH[4] & 0x0F);
	int16_t dig_H5 = (digH[5] << 4) | (digH[4] >> 4);
	int8_t dig_H6 = digH[6];

	#ifdef DEBUG
	if (bme280_print_state(file)) {
		return 1;
	}
	#endif
	
	if (bme280_set_config(file, 0, 4, 0)) {
		return 1;
	}

	if (bme280_set_control_hum(file, 5)) {
		return 1;
	}

	if (bme280_set_control_meas(file, 5, 5, 5)) {
		return 1;
	}

	#ifdef DEBUG
	if (bme280_print_state(file)) {
		return 1;
	}
	#endif
	
	usleep(113 * 1000); // 113 ms

	#ifdef DEBUG
	if (bme280_print_state(file)) {
		return 1;
	}
	#endif
	
	int32_t adc_P, adc_T, adc_H;

	if (bme280_get_readings(file, &adc_P, &adc_T, &adc_H)) {
		return 1;
	}

	BME280_S32_t t_fine;

	double T = BME280_compensate_T_double(adc_T, dig_T1, dig_T2, dig_T3, &t_fine);
	double P = BME280_compensate_P_double(adc_P, dig_P1, dig_P2, dig_P3, dig_P4, dig_P5, dig_P6, dig_P7, dig_P8, dig_P9, t_fine);
	double H = BME280_compensate_H_double(adc_H, dig_H1, dig_H2, dig_H3, dig_H4, dig_H5, dig_H6, t_fine);

	#ifdef DEBUG
	fprintf(stderr, "adc_P = %08x, adc_T = %08x, adc_H = %08x\n", adc_P, adc_T, adc_H);
	fprintf(stderr, "adc_P = %u, adc_T = %u, adc_H = %u\n", adc_P, adc_T, adc_H);
	#endif

	fprintf(stderr, "T: %.4f °C\n", T);
	fprintf(stderr, "P: %.4f hPa\n", P / 100);
	fprintf(stderr, "H: %.4f %%\n", H);

	char fields[64];

	sprintf(fields, "&field1=%.4f", T);
	strcat(buf, fields);

	sprintf(fields, "&field2=%.4f", P / 100);
	strcat(buf, fields);

	sprintf(fields, "&field3=%.4f", H);
	strcat(buf, fields);

	return 0;
}

void bme280_write_data(char *buf, unsigned char i2c_id) {
	int file = i2c_connect(i2c_id, 0x77);

	if (file == -1) {
		return;
	}

	if (bme280_print_part(file)) {
		close(file);
		return;
	}
	
	bme280_get_env(file, buf);
	close(file);
}
